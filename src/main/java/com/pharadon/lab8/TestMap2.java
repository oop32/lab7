package com.pharadon.lab8;



public class TestMap2 {
    private int mapheigh = 5;
    private int mapwidth = 5;
   
    public TestMap2(int mapwidth, int mapheigh) {
        this.mapwidth = mapwidth;
        this.mapheigh = mapheigh;
    }
    public void print(){
        for (int i = 0; i < mapheigh; i++) { 
            for (int j = 0; j < mapwidth; j++) { 
                System.out.print("-"); 
            }
            System.out.println(); 
        }
        System.out.println(); 
    }

    public int getWidth() {
        return mapwidth;
    }

    public int getHeigh() {
        return mapheigh;
    }
}
