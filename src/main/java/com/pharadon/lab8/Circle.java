package com.pharadon.lab8;

public class Circle {
    private double radius ;

    public Circle(double radius){
        this.radius = radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public double calArea() {
        return radius*3.14;
    }

    public double calPerimeter() {
        return (2*3.14)*radius;
    }

    public void printArea() {
        System.out.println("Circle area is "+calArea());
    }

    public void printPerimeter() {
        System.out.println("Circle circumference is "+calPerimeter());
    }

    

}
