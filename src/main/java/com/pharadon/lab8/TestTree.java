package com.pharadon.lab8;

public class TestTree {
    private String name;
    private int x;
    private int y;

    public TestTree(String name,int x,int y){
        this.name = name;
        this.x = x;
        this.y = y;
    }

    public String getName() {
        return name;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
    public void printTree() {
        System.out.println(getName()+" " +"X = "+getX()+" "+"Y = "+getY());
    } 

}
