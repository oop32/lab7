package com.pharadon.lab8;

public class TestShapeapp {
    public static void main(String[] args) {
        Rectangle rect1 = new Rectangle(10, 5); //width ไป heigh
        Rectangle rect2 = new Rectangle(5, 3);
        Circle circle1 = new Circle(1); // ใส่เส้นศูนย์กลาง
        Circle circle2 = new Circle(2);
        Triangle triangle = new Triangle(5, 5, 6);
        rect1.printArea(); //
        rect2.printArea();
        circle1.printArea();
        circle2.printArea();
        triangle.calArea();//สามเหลี่ยม
        rect1.printPerimeter();//เส้นรอบรูปสี่เหลี่ยม 1
        rect2.printPerimeter();//เส้นรอบรูปสี่เหลี่ยม 2
        circle1.printPerimeter();//เส้นรอบรูปวงกลม 1
        circle2.printPerimeter();//เส้นรอบรูปวงกลม 2
        triangle.calPerimeter();

    }
}
