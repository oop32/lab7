package com.pharadon.lab8;

public class Rectangle {
    private double width;
    private double heigh;

    public Rectangle(int width, int height) {
        this.width = width;
        this.heigh = height;
    }

    public double getWidth() {
        return width;
    }

    public double getHeigh() {
        return heigh;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public void setHeigh(double width) {
        this.width = heigh;
    }

    public double calArea() {
        return width*heigh;
    }

    public double calPerimeter() {
        return (width+heigh)*2;
    }

    public void printArea() {
        System.out.println("rectangle area is " + calArea());
    }

    public void printPerimeter() {
        System.out.println("Perimeter rectangle is " + calPerimeter());// ความยาวรอบรูปสี่เหลี่ยม
    }

}
