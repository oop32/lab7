package com.pharadon.lab8;

public class TestMap1 {
    private int mapheigh;
    private int mapwidth;
   
    public TestMap1(int mapwidth,int mapheigh) {  
        this.mapheigh = mapheigh;
        this.mapwidth = mapheigh;
    }

    public TestMap1() {   // Overload Constructor ค่า default ของ width และ heigh คือ 5
        this.mapheigh = 5;
        this.mapwidth = 5;
    }


    public int getWidth() {
        return mapwidth;
    }

    public int getHeigh() {
        return mapheigh;
    }
    
    public void print(){
        for (int i = 0; i < mapheigh; i++) { 
            for (int j = 0; j < mapwidth; j++) { 
                System.out.print("-"); 
            }
            System.out.println(); 
        }
        System.out.println(); 
    }
    
}
