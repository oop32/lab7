package com.pharadon.lab8;


public class Triangle {
    private double a;
    private double b;
    private double c;
    private double s;

    public Triangle(double a,double b ,double c){
        this.a = a;
        this.b = b;
        this.c = c;
        
    }

    public double getA() {
        return a ;
    }

    public double getB() {
        return b ;
    }
    
    public double getC() {
        return c ;
    }

    public void setA(double a) {
        this.a = a;
    }
    
    public void setB(double b) {
        this.b = b;
    }

    public void setC(double c) {
        this.c = c;
    }

    

    public double calArea() {
        s = (a + c + b)/2;
        return
        Math.sqrt(s*(s-a)*(s-b)*(s-c));
       
    }

    public double calPerimeter(){
        return a + b + c;
    }

    public void printArea(){
        System.out.println("Triangle area = "+ calPerimeter() );
    }
     

    public void printPeimmeter(){
        System.out.println("Circumference of a triangle is "+calPerimeter());
    }

   
}

// d = a+b+c /2
// sqrt s*(s-a)*(s-b)*(s-c)